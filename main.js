'use strict';

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const closeButtonModal = document.querySelector('.modal-close-button');
const showButtonModal = document.querySelector('.modal-show-button');

const closeModal = function () {
    overlay.classList.add('hidden');
    modal.classList.add('hidden');
}

const openModal = function () {
    overlay.classList.remove('hidden');
    modal.classList.remove('hidden');
}

showButtonModal.addEventListener('click', openModal);
closeButtonModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);
document.addEventListener('keydown', function (event) {
    if (event.key === 'Escape' && !modal.classList.contains('hidden')) {
        closeModal();
    }
});